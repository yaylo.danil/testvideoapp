package com.yaylo.testvideoapp.viewmodel.player

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS
import com.arthenica.mobileffmpeg.FFmpeg
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class VideoPlayerViewModel : ViewModel() {

    private var mergedVideoUri_: MutableLiveData<String> = MutableLiveData()
    val mergedVideoUri: LiveData<String> = mergedVideoUri_

    private val BEGIN_VIDEO_TIME = "00:00:00"

    private val FIRST_CUT = "firstCut.mp4"
    private val SECOND_CUT = "secondCut.mp4"
    private val MERGED = "mergedVideo.mp4"
    private val INPUT = "input.mp4"
    private val LIST = "list.txt"

    fun trimAndConcatInReverseVideo(context: Context, inputUri: String) {
        try {
            val storagePath = context.filesDir
            val uriPath = context.contentResolver.openInputStream(inputUri.toUri())?.let {
                val f = File(storagePath, INPUT)
                it.copyTo(FileOutputStream(f), it.available())
                it.close()
                f.absolutePath
            } ?: ""
            val videoDurations = retrieveVideoDuration(context, inputUri)

            File(storagePath, LIST).printWriter().use {
                it.println("file '$storagePath/$SECOND_CUT'")
                it.println("file '$storagePath/$FIRST_CUT'")
            }

            val commandCutFirst =
                "-i $uriPath -ss $BEGIN_VIDEO_TIME -t ${videoDurations.first} $storagePath/$FIRST_CUT"
            val commandCutSecond =
                "-i $uriPath -ss ${videoDurations.first} -t ${videoDurations.second} $storagePath/$SECOND_CUT"
            val commandMerge =
                "-f concat -safe 0 -i $storagePath/$LIST -c copy $storagePath/$MERGED"

            viewModelScope.launch {
                FFmpeg.executeAsync(commandCutFirst) { _, code ->
                    if (code == RETURN_CODE_SUCCESS) {
                        FFmpeg.executeAsync(commandCutSecond) { _, code1 ->
                            if (code1 == RETURN_CODE_SUCCESS) {
                                FFmpeg.executeAsync(commandMerge) { _, code2 ->
                                    if (code2 == RETURN_CODE_SUCCESS) {
                                        clearTempFiles(storagePath)
                                        mergedVideoUri_.postValue(
                                            Uri.fromFile(File(storagePath, MERGED)).toString()
                                        )
                                    } else mergedVideoUri_.postValue(inputUri)
                                }
                            } else mergedVideoUri_.postValue(inputUri)
                        }
                    } else mergedVideoUri_.postValue(inputUri)
                }
            }

        } catch (e: java.lang.Exception) {
            Log.e(this.javaClass.name, e.toString())
            mergedVideoUri_.postValue(inputUri)
        }
    }

    private fun retrieveVideoDuration(context: Context, uri: String): Pair<String, String> {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, uri.toUri())
        val duration =
            retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toLong() ?: 0
        val halfDuration = duration / 2
        val durationFormatted = formatTime(duration)
        val halfDurationFormatted = formatTime(halfDuration)

        retriever.release()
        return Pair(halfDurationFormatted, durationFormatted)
    }

    private fun formatTime(time: Long) = DateTimeFormatter.ofPattern("HH:mm:ss")
        .withZone(ZoneId.of("UTC"))
        .format(Instant.ofEpochMilli(time))

    fun clearTempFiles(storagePath: File) {
        File(storagePath, FIRST_CUT).apply {
            if (exists())
                delete()
        }
        File(storagePath, SECOND_CUT).apply {
            if (exists())
                delete()
        }
        File(storagePath, LIST).apply {
            if (exists())
                delete()
        }
        File(storagePath, INPUT).apply {
            if (exists())
                delete()
        }
    }

    fun clearMergedVideoFile(storagePath: File) {
        File(storagePath, MERGED).apply {
            if (exists())
                delete()
        }
    }
}
