package com.yaylo.testvideoapp.viewmodel.selector.view.selector

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.yaylo.testvideoapp.R
import com.yaylo.testvideoapp.databinding.FragmentVideoSelectorBinding
import com.yaylo.testvideoapp.viewmodel.selector.VideoSelectorViewModel


class VideoSelectorFragment : Fragment() {

    private var _binding: FragmentVideoSelectorBinding? = null
    private val binding get() = _binding!!

    private val viewModel: VideoSelectorViewModel by viewModels()

    private lateinit var getContentCaller: ActivityResultLauncher<String>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getContentCaller = registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            if (uri.toString().isNotEmpty()) {
                findNavController().navigate(
                    R.id.action_videoSelectorFragment_to_videoPlayerFragment,
                    bundleOf(VIDEO_URI_TAG to uri.toString())
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVideoSelectorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.selectVideoButton.setOnClickListener {
            getContentCaller.launch("video/*")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val VIDEO_URI_TAG = "videoUriTag"
    }
}
