package com.yaylo.testvideoapp.viewmodel.selector.view.player

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.Player.Listener
import com.yaylo.testvideoapp.databinding.FragmentVideoPlayerBinding
import com.yaylo.testvideoapp.viewmodel.selector.view.selector.VideoSelectorFragment
import com.yaylo.testvideoapp.viewmodel.player.VideoPlayerViewModel

class VideoPlayerFragment : Fragment() {

    private var _binding: FragmentVideoPlayerBinding? = null
    private val binding get() = _binding!!

    private val viewModel: VideoPlayerViewModel by viewModels()

    private var videoUri = ""

    private lateinit var askPermissionCaller: ActivityResultLauncher<String>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        askPermissionCaller =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                videoUri = arguments?.getString(VideoSelectorFragment.VIDEO_URI_TAG) ?: ""
                if (isGranted) {
                    viewModel.trimAndConcatInReverseVideo(requireContext(), videoUri)
                } else {
                    setupPlayer()
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVideoPlayerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        askPermissionCaller.launch(WRITE_EXTERNAL_STORAGE)
        observedMergedVideoUri()
    }

    private fun observedMergedVideoUri() {
        viewModel.mergedVideoUri.observe(viewLifecycleOwner) {
            videoUri = it
            setupPlayer()
        }
    }

    private fun setupPlayer() {
        binding.playerProgressBar.visibility = View.GONE

        val player = ExoPlayer.Builder(requireContext()).build()
        binding.mediaPlayer.player = player
        val mediaItem = MediaItem.fromUri(videoUri)
        player.apply {
            setMediaItem(mediaItem)
            prepare()
            play()
            addListener(object : Listener {
                override fun onPlaybackStateChanged(playbackState: Int) {
                    super.onPlaybackStateChanged(playbackState)
                    if (playbackState == Player.STATE_ENDED) {
                        viewModel.clearMergedVideoFile(requireContext().filesDir)
                        findNavController().popBackStack()
                    }
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.clearMergedVideoFile(requireContext().filesDir)
        viewModel.clearTempFiles(requireContext().filesDir)
        _binding = null
    }

}