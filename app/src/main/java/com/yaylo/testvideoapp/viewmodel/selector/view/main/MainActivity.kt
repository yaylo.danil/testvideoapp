package com.yaylo.testvideoapp.viewmodel.selector.view.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yaylo.testvideoapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
}
